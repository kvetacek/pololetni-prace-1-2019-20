/*

Napište program, který bude vypisovat posloupnost (celých) čísel.

Program půjde spustit ve 3 režimech podle počtu zadaných parametrů.

S jedním parametrem: program vypíše čísla od 1 do daného parametru.

S dvěma parametry: program vypíše čísla mezi 2 zadanými.

S třemi parametry: program vypíše čísla mezi prvním a třetím parametrem s
krokem určeným druhým parametrem.

Ukázkové spuštění (všimněte si, že pro některé vstupy program nic nevypíše):

> java Seq 2
1 2
> java Seq -1
> java Seq 2 5
2 3 4 5
> java Seq -2 3
-2 -1 0 1 2 3
> java Seq 5 0
> java Seq 2 2 12
2 4 6 8 10 12
> java Seq 2 3 18
2 5 8 11 14 17
> java Seq 17 5 12
> java Seq 14 -2 8
14 12 10 8

Rozšíření (1b): Pokud je první argument -s, je druhý argument brán jako
oddělovač místo mezery.


> java Seq -s , 5
1,2,3,4,5
> java Seq -s ::: 4 3 17
4:::7:::10:::13:::16

*/
public class Seq {
    public static void main(String args[]) {
    }
}
