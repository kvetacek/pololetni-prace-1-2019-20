/*

Napište program pro výpočet
[Pascalova trojúhelníku](https://cs.wikipedia.org/wiki/Pascal%C5%AFv_troj%C3%BAheln%C3%ADk).

Uživatel zadá jako parametr programu počet řádků.
V základním řešení není nutné řešit vycentrování výstupu.

java Pascal 6
1
1 1
1 2 1
1 3 3 1
1 4 6 4 1
1 5 10 10 5 1

Rozšíření (1b): Program bude vypisovat Pascalův trojúhelník zarovnaný na střed.

java Pascal 6
          1
        1   1
      1   2   1
    1   3   3   1
  1   4   6   4   1
1   5  10  10   5   1


*/
public class Pascal {
    public static void main(String args[]) {
    }
}
